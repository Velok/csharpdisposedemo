public class BaseWithManagedAndUnmanagedResources : IDisposable
{
    private bool _isDisposed;
    private IntPtr _pointer;

    public BaseWithManagedAndUnmanagedResources()
    {
        _pointer = Marshal.AllocHGlobal(1024);
        EventProviderClass.SampleEventRaised += OnSampleEventRaised;
    }

    // Finalizer welcher ausschliesslich von der Runtime zu einem nicht definierten Zeitpunkt aufgerufen wird
    // nachdem das Objekt vom GC der Liste der zu finalisierenden Objekte hinzugefügt wurde.
    // Siehe https://www.dotnettricks.com/learn/netframework/net-garbage-collection-and-finalization-queue
    ~BaseWithManagedAndUnmanagedResources()
    {
        // Aufruf mit dem Flag disposing = false damit vom Finalizer aus
        // nur noch die unmanaged Resourcen freigegeben werden.
        Dispose(false);
    }

    // Methode aus dem IDisposable Interface welche explizit aus dem Code aufgerufen werden soll.
    public void Dispose()
    {
        // Aufruf mit dem Flag disposing = true damit vom Code aus
        // sowohl die managed als auch die unmanaged Resourcen freigegeben werden.
        Dispose(true);
        // Dem GC mitteilen dass dieses Objekt komplett aufgeräumt wurde und deshalb
        // kein Finalize mehr nötig ist um unmanaged Resourcen freizugeben.
        // Siehe: https://stackoverflow.com/questions/151051/when-should-i-use-gc-suppressfinalize/151244#151244
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        // Sicherstellen dass nur einmal die Resourcen freigegeben werden.
        if (_isDisposed)
        {
            return;
        }
        _isDisposed = true;

        if (disposing)
        {
            // Freigeben von managed Resourcen.
            EventProviderClass.SampleEventRaised -= OnSampleEventRaised;
        }

        // Freigeben von unmanaged Resourcen.
        Marshal.FreeHGlobal(_pointer);
        _pointer = IntPtr.Zero;
    }
}