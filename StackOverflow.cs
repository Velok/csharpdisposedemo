// Base class with only managed resources
public class Base : IDisposable
{
    private bool _isDisposed;

    public Base()
    {
        // Construction
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (_isDisposed)
        {
            return;
        }
        _isDisposed = true;

        if (disposing)
        {
            // Release managed resources
        }
    }
}

// First derived class with managed and unmanaged resources
public class DerivedA : Base
{
    public DerivedA()
    {
        // Construction
    }

    // Only one Finalizer necessary in a inheritance chain when unmanaged
    // resources always are freed via the Dispose(bool disposing) method?
    ~DerivedA()
    {
        Dispose(false);
    }

    protected override void Dispose(bool disposing)
    {
        if (_isDisposed)
        {
            return;
        }
        _isDisposed = true;

        if (disposing)
        {
            // Release managed resources
        }

        // Release unmanaged resources
    }
}

// Second derived class with managed and unmanaged resources
public class DerivedB : DerivedA
{
    public DerivedB()
    {
        // Construction
    }

    protected override void Dispose(bool disposing)
    {
        if (_isDisposed)
        {
            return;
        }
        _isDisposed = true;

        if (disposing)
        {
            // Release managed resources
        }

        // Release unmanaged resources
    }
}